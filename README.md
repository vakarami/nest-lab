## Description
NestJS Microservices Playground

This repository serves as a comprehensive playground to explore the power of NestJS for building robust microservices architectures. It showcases a practical implementation of three interconnected microservices – Order, Billing, and Auth – demonstrating key NestJS features:

* **Event-Driven Communication with RabbitMQ**: Experience seamless message exchange between services using RabbitMQ, a powerful messaging broker.
* **Advanced Security with Guards**: Implement robust authorization mechanisms by leveraging NestJS Guards.


## Getting Started (Dockerized):

* Clone the repository: `git clone git@gitlab.com:vakarami/nest-lab.git`
* Start the services with Docker Compose: `docker-compose up -d`

This will spin up a MongoDB instance, a RabbitMQ container, and the three NestJS microservices.

## Beyond the Basics:

This project delves deeper into NestJS, providing a solid foundation for building complex and scalable microservices applications. You'll encounter valuable insights into:

* **Microservice Architecture Principles**: Understand the concepts behind microservices and their efficient implementation.
* **Best Practices for NestJS Development**: Discover recommended patterns and approaches for building well-structured NestJS applications.

## Let's Explore!

Dive into the codebase and experiment with the different functionalities. Feel free to extend and customize the microservices to suit your specific needs.


## License

Nest is [MIT licensed](LICENSE).
