import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { BILLING_SERVICE } from './constants/services';
import { CreateOrderRequest } from './dto/create-order.request';
import { OrdersRepository } from './orders.repository';

@Injectable()
export class OrdersService {
  constructor(
    private readonly ordersRepository: OrdersRepository,
    @Inject(BILLING_SERVICE) private billingClient: ClientProxy,
  ) {}

  async createOrder(request: CreateOrderRequest, userId: string) {
    try {
      const data = { ...request, userId };
      const order = await this.ordersRepository.create(data);
      await lastValueFrom(this.billingClient.emit('order_created', data));
      return order;
    } catch (err) {
      throw err;
    }
  }

  async getOrders(userId: string) {
    return this.ordersRepository.find({ userId });
  }
}
