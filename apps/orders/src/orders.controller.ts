import { Body, Controller, Get, Post, Req, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '@app/common';
import { CreateOrderRequest } from './dto/create-order.request';
import { OrdersService } from './orders.service';

@Controller('orders')
export class OrdersController {
  constructor(private readonly ordersService: OrdersService) {}

  @Post()
  @UseGuards(JwtAuthGuard)
  async createOrder(@Body() request: CreateOrderRequest, @Req() req: any) {
    const userId = req.user?._id;
    return this.ordersService.createOrder(request, userId);
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  async getOrders(@Req() req: any) {
    const userId = req.user?._id;
    return this.ordersService.getOrders(userId);
  }
}
